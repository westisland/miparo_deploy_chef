script "delete_apache_and_php" do
  interpreter "bash"
  user "root"
  cwd "/tmp/"
  code <<-EOH
    echo 1 >/tmp/script.run
	yum -y erase httpd httpd-tools apr apr-util php-*
    echo 2 >>/tmp/script.run
  EOH
end

service 'httpd stop' do
  service_name 'httpd'
  action :stop
end

package 'php56' do
package_name 'php56'
action:install
end
package 'php56-xml' do
package_name 'php56-xml'
action:install
end
package 'php56-xmlrpc' do
package_name 'php56-xmlrpc'
action:install
end
package 'php56-soap' do
package_name 'php56-soap'
action:install
end
package 'php56-gd' do
package_name 'php56-gd'
action:install
end
package 'php56-mysqlnd' do
package_name 'php56-mysqlnd'
action:install
end
package 'php56-mbstring' do
package_name 'php56-mbstring'
action:install
end
package 'redis' do
package_name 'redis'
action:install
end
service 'redisstart' do
  service_name 'redis'
  action :start
end
