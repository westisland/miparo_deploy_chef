
template "/srv/www/miparo/current/.env" do
  source ".env"
  mode 0644
end


script "fix_permissions" do
  interpreter "bash"
  user "root"
  cwd "/srv/www/miparo/current/"
  code <<-EOH
	mkdir logs
	mkdir resources/lang
	chmod 777 logs
	mkdir storage/logs
	mkdir storage/framework
	mkdir mkdir /cloudwatch
	chmod -R 777 bootstrap/cache/*
	chmod 777 storage/framework/*
	chmod 777 storage/framework/*
	chmod 777 resources/lang*
	chmod -R 777 storage/*
	chmod -R 777 storage/logs/*
	chmod 777 storage/logs/*
	chmod -R 777 bootstrap/cache
	chmod -R 777 bootstrap/cache/*
	
	cd /tmp/
	echo "add-apt-repository --yes ppa:ondrej/php" > setupserver.sh
	echo "apt-get -y update" >> setupserver.sh
	echo "apt-get install -y php5.6 php5.6-mcrypt php5.6-mbstring php5.6-curl php5.6-cli php5.6-mysql php5.6-gd php5.6-intl php5.6-xsl php5.6-zip libapache2-mod-php5.6" >> setupserver.sh
    echo "apt-get autoremove -y" >>setupserver.sh
    echo "cd /srv/www/miparo/current/" >>setupserver.sh
    echo "curl -sS https://getcomposer.org/installer  | php" >>setupserver.sh
    echo "php composer.phar update " >>setupserver.sh
    echo "php composer.phar update " >>setupserver.sh
    echo "apt-get -y install libapache2-mod-php5.6 " >>setupserver.sh
    echo "a2dismod php5 " >>setupserver.sh
    echo "a2enmod php5.6 " >>setupserver.sh
    echo "apt-get -y install -y redis-server " >>setupserver.sh
    echo "service redis-server restart" >>setupserver.sh
    echo "apt-get -y --purge remove php5-common " >>setupserver.sh
    echo "apt-get -y install libapache2-mod-php5.6 " >>setupserver.sh
    echo "apt-get -y upgrade " >>setupserver.sh
    echo "apt-get -y autoremove " >>setupserver.sh
    echo "service apache2 restart " >>setupserver.sh
    echo "apt-get install -y unzip " >>setupserver.sh
    echo "apt-get install -y libwww-perl libdatetime-perl " >>setupserver.sh

    echo "php artisan optimize " >>setupserver.sh
    echo "php artisan migrate " >>setupserver.sh
   echo "php artisan localisation:import seolinks" >>setupserver.sh
	echo "php artisan localisation:import auth" >>setupserver.sh
	echo "php artisan localisation:import passwords" >>setupserver.sh
	echo "php artisan localisation:import pagination" >>setupserver.sh
	echo "php artisan localisation:import viewproperty" >>setupserver.sh
	echo "php artisan localisation:import common" >>setupserver.sh

    echo "mkdir /cloudwatch " >>setupserver.sh
    echo "cd /cloudwatch " >>setupserver.sh
    echo "curl http://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.1.zip -O ">>setupserver.sh
	echo "unzip CloudWatchMonitoringScripts-1.2.1.zip ">>setupserver.sh
	echo "rm CloudWatchMonitoringScripts-1.2.1.zip ">>setupserver.sh
	echo "cd aws-scripts-mon " >>setupserver.sh 
    echo "AWSAccessKeyId=AKIAJQZWPDMPYHFMHXQQ" >/cloudwatch/aws.cred
    echo "AWSSecretKey=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 " >>/cloudwatch/aws.cred
    echo " " >>setupserver.sh
    echo " " >>setupserver.sh
	
	
	
	EOH
end


cron "gitpull" do
  hour "*"
  minute "1"
  weekday "*"
  command "cd /srv/www/miparo/current && git pull"
end

cron "schedule" do
  hour "*"
  minute "1"
  weekday "*"
  command "cd /srv/www/miparo/current && php artisan schedule:run"
end

script "fix_permissions" do
  interpreter "bash"
  user "root"
  cwd "/srv/www/miparo/current/"
  code <<-EOH
	
	
    php composer.phar update 
    php composer.phar install 

	php artisan localisation:import seolinks
	php artisan localisation:import auth
	php artisan localisation:import passwords
	php artisan localisation:import pagination
	php artisan localisation:import viewproperty
	php artisan localisation:import common

	php artisan optimize
    php artisan migrate
	
	EOH
end



